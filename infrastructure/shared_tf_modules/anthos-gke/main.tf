# Copyright 2020 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# GKE
locals {
  # The following locals are derived from the subnet object
  node_subnet        = var.subnet.name
  pod_subnet         = var.subnet.secondary_ip_range[0].range_name
  svc_subnet         = var.subnet.secondary_ip_range[local.suffix].range_name
  region             = var.subnet.region
  network            = split("/", var.subnet.network)[length(split("/", var.subnet.network)) - 1]
  network_project_id = var.subnet.project
  suffix             = var.suffix
  env                = var.env
  zone               = var.zone
}

data "google_project" "project" {
  project_id = var.project_id
}

module "gke" {
  source                   = "terraform-google-modules/kubernetes-engine/google//modules/beta-public-cluster"
  version                  = "13.0.0"
  project_id               = var.project_id
  name                     = "gke-${local.env}-${local.region}${local.zone}-${local.suffix}"
  regional                 = false
  region                   = local.region
  zones                    = ["${local.region}-${local.zone}"]
  release_channel          = "REGULAR"
  network                  = local.network
  subnetwork               = local.node_subnet
  network_project_id       = local.network_project_id
  ip_range_pods            = local.pod_subnet
  ip_range_services        = local.svc_subnet
  remove_default_node_pool = true
  cluster_resource_labels  = { "mesh_id" : "proj-${data.google_project.project.number}", "environ" : local.env, "infra" : "gcp" }

  node_pools = [
    {
      name         = "node-pool-01"
      machine_type = "e2-standard-4"
      min_count    = 4
      max_count    = 10
      auto_upgrade = true
      node_count   = 4
    },
  ]
}

module "acm" {
  source           = "github.com/CloudPharaoh/terraform-google-kubernetes-engine//modules/acm"
  # version          = "13.0.0"
  project_id       = var.project_id
  cluster_name     = module.gke.name
  location         = module.gke.location
  cluster_endpoint = module.gke.endpoint
  create_ssh_key   = false
  sync_repo        = var.acm_sync_repo_url
  secret_type      = "gcenode"
}

module "hub" {
  source                  = "terraform-google-modules/kubernetes-engine/google//modules/hub"
  version                 = "13.0.0"
  module_depends_on       = [module.acm.wait]
  project_id              = var.project_id
  cluster_name            = module.gke.name
  location                = module.gke.location
  cluster_endpoint        = module.gke.endpoint
  gke_hub_membership_name = module.gke.name
  gke_hub_sa_name         = "hub-sa-${local.env}-${local.region}${local.zone}"
}

# The following are only required for using CSR repo (secret type: gcenode) with ACM on a GKE cluster with workload identity enabled.
# If using a different secret type these are not needed.
resource "google_sourcerepo_repository_iam_member" "sourcerepo_iam" {
  project    = var.project_id
  repository = var.acm_repo_name
  role       = "roles/source.reader"
  member     = "serviceAccount:${module.gke.service_account}"
}

module "service_account_iam_binding" {
  source  = "terraform-google-modules/iam/google//modules/service_accounts_iam"
  version = "6.4.1"

  service_accounts = [
    module.gke.service_account
  ]
  project = var.project_id
  mode    = "additive"
  bindings = {
    "roles/iam.workloadIdentityUser" = [
      "serviceAccount:${var.project_id}.svc.id.goog[config-management-system/importer]"
    ]
  }
}

module "kubectl-gke" {
  source                  = "terraform-google-modules/gcloud/google//modules/kubectl-wrapper"
  version                 = "2.0.3"
  module_depends_on       = [module.acm]
  project_id              = var.project_id
  cluster_name            = module.gke.name
  cluster_location        = module.gke.location
  kubectl_create_command  = "kubectl annotate serviceaccount -n config-management-system importer iam.gke.io/gcp-service-account=${module.gke.service_account}"
  kubectl_destroy_command = "kubectl annotate serviceaccount -n config-management-system importer iam.gke.io/gcp-service-account-"
  skip_download           = true
}
