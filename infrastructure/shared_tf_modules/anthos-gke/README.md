# GKE for Anthos module
Registers cluster to hub and installs ACM.
<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| acm\_repo\_name | Name of ACM repository. | string | n/a | yes |
| acm\_repo\_project | Project ID where ACM repository exists. | string | n/a | yes |
| acm\_sync\_repo\_url | URL for ACM sync repo. Requires Cloud Source Repositories for this implementation. | string | n/a | yes |
| env |  | string | n/a | yes |
| project\_id | Project ID where GKE cluster is to be created. | string | n/a | yes |
| subnet |  | object | n/a | yes |
| suffix |  | number | n/a | yes |
| zone | zone to create GKE cluster in. | string | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| endpoint |  |
| location |  |
| name |  |
| service\_account |  |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->