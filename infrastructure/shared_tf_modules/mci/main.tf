/**
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

data "google_project" "project" {
  project_id = var.project_id
}

module "mci" {
  source  = "terraform-google-modules/gcloud/google"
  version = "~> 2.0"

  platform = "linux"
  upgrade  = true
  additional_components = ["alpha"]

  create_cmd_entrypoint  = "${path.module}/scripts/enable_mci.sh"
  create_cmd_body        = "${var.project_id} ${var.cluster_name}"
  destroy_cmd_entrypoint = "gcloud"
  destroy_cmd_body       = "alpha container hub ingress disable --force --project ${var.project_id}"
}


# create firewall rules to allow healthchecks for mci to work
# https://cloud.google.com/kubernetes-engine/docs/how-to/multi-cluster-ingress-setup#shared_vpc_deployment
resource "google_compute_firewall" "mci-l7" {
  name    = "mci-l7-ingress"
  network = var.network_name
  project = var.network_project

  allow {
    protocol = "tcp"
    ports    = ["0-65535"]
  }

  source_ranges = ["130.211.0.0/22", "35.191.0.0/16"]
}
