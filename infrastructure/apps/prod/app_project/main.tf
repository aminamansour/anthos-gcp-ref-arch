# Copyright 2020 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

module "app_project" {
  source               = "terraform-google-modules/project-factory/google"
  version              = "10.1.0"
  name                 = var.app_project_name
  random_project_id    = true

  org_id               = var.org_id
  folder_id            = var.folder_id
  billing_account      = var.billing_account

  svpc_host_project_id = data.terraform_remote_state.host_project.outputs.project_id
  shared_vpc_subnets   = data.terraform_remote_state.shared_vpc.outputs.subnets_self_links

  activate_apis = [
    "compute.googleapis.com",
    "container.googleapis.com",
    "sqladmin.googleapis.com",
  ]

  disable_services_on_destroy = "false"
}

module "shared_vpc_access_app" {
  source             = "terraform-google-modules/project-factory/google//modules/shared_vpc_access"
  version            = "10.1.0"
  host_project_id    = data.terraform_remote_state.host_project.outputs.project_id
  service_project_id = module.app_project.project_id
  enable_shared_vpc_service_project = true
  active_apis = [
    "compute.googleapis.com"
  ]
  shared_vpc_subnets = data.terraform_remote_state.shared_vpc.outputs.subnets_self_links
}