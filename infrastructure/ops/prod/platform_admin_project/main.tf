# Copyright 2020 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

module "platform_admin_project" {
  source               = "terraform-google-modules/project-factory/google"
  version              = "10.1.0"
  name                 = var.platform_admin_project
  random_project_id    = true

  org_id               = var.org_id
  folder_id            = var.folder_id
  billing_account      = var.billing_account

  svpc_host_project_id = data.terraform_remote_state.host_project.outputs.project_id
  shared_vpc_subnets   = data.terraform_remote_state.shared_vpc.outputs.subnets_self_links

  activate_apis = [
    "compute.googleapis.com",
    "container.googleapis.com",
    "gkeconnect.googleapis.com",
    "gkehub.googleapis.com",
    "cloudresourcemanager.googleapis.com",
    "anthos.googleapis.com",
    "multiclusteringress.googleapis.com",
    "sourcerepo.googleapis.com"
  ]

  disable_services_on_destroy = "false"
}

module "shared_vpc_access_platform_admin" {
  source              = "terraform-google-modules/project-factory/google//modules/shared_vpc_access"
  version             = "10.1.0"
  host_project_id     = data.terraform_remote_state.host_project.outputs.project_id
  service_project_id  = module.platform_admin_project.project_id
  grant_services_security_admin_role = true
  enable_shared_vpc_service_project = true
  active_apis         = [
    "compute.googleapis.com",
    "container.googleapis.com"
  ]
  shared_vpc_subnets  = data.terraform_remote_state.shared_vpc.outputs.subnets_self_links
}

# Grant project editor to the passed user
resource "google_project_iam_member" "platform_admin_project_editor" {
  project = module.platform_admin_project.project_id
  role    = "roles/editor"
  member  = "user:${var.project_editor}"
}

# Grant source repo admin to the passed user
resource "google_project_iam_member" "platform_admin_project_source_admin" {
  project = module.platform_admin_project.project_id
  role    = "roles/source.admin"
  member  = "user:${var.project_editor}"
}
