#!/bin/bash

# Get TF_CLOUDBUILD_SA
export TF_CLOUDBUILD_SA=$(gcloud projects describe $PROJECT_ID --format='value(projectNumber)')@cloudbuild.gserviceaccount.com

# Install nomos
gsutil cp gs://config-management-release/released/latest/linux_amd64/nomos nomos
chmod +x nomos

# Init repo
mkdir acm_repo
cd acm_repo
git init
git remote add origin $REPO_URL
../nomos init
git config --local user.email ${TF_CLOUDBUILD_SA}
git config --local user.name 'terraform'
git config --local credential.'https://source.developers.google.com'.helper gcloud.sh
git add .
git commit -m "init repo"
git push -u origin master