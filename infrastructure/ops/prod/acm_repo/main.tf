# Copyright 2020 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

locals {
  project_id = data.terraform_remote_state.platform_admin_project.outputs.project_id
}

resource "google_sourcerepo_repository" "acm_repo" {
    name = "acm_repo"
    project = local.project_id
}

resource "null_resource" "init_acm_repo" {
  depends_on = [
    google_sourcerepo_repository.acm_repo
  ]

  provisioner "local-exec" {
    interpreter = ["bash", "-exc"]
    command     = "${path.module}/scripts/init_repo.sh"
    environment = {
      PROJECT_ID = local.project_id
      REPO_URL   = google_sourcerepo_repository.acm_repo.url
    }
  }

  triggers = {
    script_sha1 = sha1(file("${path.module}/scripts/init_repo.sh")),
  }
}
