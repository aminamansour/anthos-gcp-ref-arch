# Copyright 2020 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

output "gke_1_name" { value = module.create_gke_1.name }
output "gke_1_location" { value = module.create_gke_1.location }
output "gke_1_endpoint" { value = module.create_gke_1.endpoint }
output "gke_1_sa" { value = module.create_gke_1.service_account }

output "gke_2_name" { value = module.create_gke_2.name }
output "gke_2_location" { value = module.create_gke_2.location }
output "gke_2_endpoint" { value = module.create_gke_2.endpoint }
output "gke_2_sa" { value = module.create_gke_2.service_account }


output "gke_3_name" { value = module.create_gke_3.name }
output "gke_3_location" { value = module.create_gke_3.location }
output "gke_3_endpoint" { value = module.create_gke_3.endpoint }
output "gke_3_sa" { value = module.create_gke_3.service_account }

output "gke_4_name" { value = module.create_gke_4.name }
output "gke_4_location" { value = module.create_gke_4.location }
output "gke_4_endpoint" { value = module.create_gke_4.endpoint }
output "gke_4_sa" { value = module.create_gke_4.service_account }
