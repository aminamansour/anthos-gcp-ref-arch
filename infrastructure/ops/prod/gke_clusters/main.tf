# Copyright 2020 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Create GKE zonal cluster in platform_admin project using subnet-01 zone a
module "create_gke_1" {
  source            = "../../../shared_tf_modules/anthos-gke"
  subnet            = data.terraform_remote_state.shared_vpc.outputs.subnets["${var.subnet_01_region}/${var.subnet_01_name}"]
  project_id        = data.terraform_remote_state.platform_admin_project.outputs.project_id
  suffix            = "1"
  zone              = "a"
  env               = var.env
  acm_sync_repo_url = data.terraform_remote_state.acm_repo.outputs.url
  acm_repo_name     = data.terraform_remote_state.acm_repo.outputs.name
  acm_repo_project  = data.terraform_remote_state.acm_repo.outputs.project
}

# Create GKE zonal cluster in platform_admin project using subnet-01 zone b
module "create_gke_2" {
  source            = "../../../shared_tf_modules/anthos-gke"
  subnet            = data.terraform_remote_state.shared_vpc.outputs.subnets["${var.subnet_01_region}/${var.subnet_01_name}"]
  project_id        = data.terraform_remote_state.platform_admin_project.outputs.project_id
  suffix            = "2"
  zone              = "b"
  env               = var.env
  acm_sync_repo_url = data.terraform_remote_state.acm_repo.outputs.url
  acm_repo_name     = data.terraform_remote_state.acm_repo.outputs.name
  acm_repo_project  = data.terraform_remote_state.acm_repo.outputs.project
}


# Create GKE zonal cluster in platform_admin project using subnet-02 zone a
module "create_gke_3" {
  source            = "../../../shared_tf_modules/anthos-gke"
  subnet            = data.terraform_remote_state.shared_vpc.outputs.subnets["${var.subnet_02_region}/${var.subnet_02_name}"]
  project_id        = data.terraform_remote_state.platform_admin_project.outputs.project_id
  suffix            = "1"
  zone              = "a"
  env               = var.env
  acm_sync_repo_url = data.terraform_remote_state.acm_repo.outputs.url
  acm_repo_name     = data.terraform_remote_state.acm_repo.outputs.name
  acm_repo_project  = data.terraform_remote_state.acm_repo.outputs.project
}

# Create GKE zonal cluster in platform_admin project using subnet-02 zone b
module "create_gke_4" {
  source            = "../../../shared_tf_modules/anthos-gke"
  subnet            = data.terraform_remote_state.shared_vpc.outputs.subnets["${var.subnet_02_region}/${var.subnet_02_name}"]
  project_id        = data.terraform_remote_state.platform_admin_project.outputs.project_id
  suffix            = "2"
  zone              = "b"
  env               = var.env
  acm_sync_repo_url = data.terraform_remote_state.acm_repo.outputs.url
  acm_repo_name     = data.terraform_remote_state.acm_repo.outputs.name
  acm_repo_project  = data.terraform_remote_state.acm_repo.outputs.project
}
