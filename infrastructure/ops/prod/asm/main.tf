# Copyright 2020 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

module "asm" {
  source = "../../../shared_tf_modules/asm-gke"

  project_id     = data.terraform_remote_state.platform_admin_project.outputs.project_id
  asm_properties = var.asm_properties
  clusters       = {
    (data.terraform_remote_state.gke_clusters.outputs.gke_1_name) = {
      name = data.terraform_remote_state.gke_clusters.outputs.gke_1_name
      location = data.terraform_remote_state.gke_clusters.outputs.gke_1_location
      endpoint = data.terraform_remote_state.gke_clusters.outputs.gke_1_endpoint
      regional = false
    },
    (data.terraform_remote_state.gke_clusters.outputs.gke_2_name) = {
      name = data.terraform_remote_state.gke_clusters.outputs.gke_2_name
      location = data.terraform_remote_state.gke_clusters.outputs.gke_2_location
      endpoint = data.terraform_remote_state.gke_clusters.outputs.gke_2_endpoint
      regional = false
    },
    (data.terraform_remote_state.gke_clusters.outputs.gke_3_name) = {
      name = data.terraform_remote_state.gke_clusters.outputs.gke_3_name
      location = data.terraform_remote_state.gke_clusters.outputs.gke_3_location
      endpoint = data.terraform_remote_state.gke_clusters.outputs.gke_3_endpoint
      regional = false
    },
    (data.terraform_remote_state.gke_clusters.outputs.gke_4_name) = {
      name = data.terraform_remote_state.gke_clusters.outputs.gke_4_name
      location = data.terraform_remote_state.gke_clusters.outputs.gke_4_location
      endpoint = data.terraform_remote_state.gke_clusters.outputs.gke_4_endpoint
      regional = false
    }
  }
}