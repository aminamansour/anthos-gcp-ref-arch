module "mci-prod" {
  source             = "../../../shared_tf_modules/mci/"
  project_id         = data.terraform_remote_state.platform_admin_project.outputs.project_id
  cluster_name       = data.terraform_remote_state.gke_clusters.outputs.gke_1_name
  network_name       = data.terraform_remote_state.shared_vpc.outputs.network_name
  network_project    = data.terraform_remote_state.host_project.outputs.project_id
}
