output "subnets_self_links" { value = module.shared_vpc.subnets_self_links }
output "subnets_names" { value = module.shared_vpc.subnets_names }
output "network_name" { value = module.shared_vpc.network_name }
output "network_self_link" { value = module.shared_vpc.network_self_link }
output "subnets" { value = module.shared_vpc.subnets }
