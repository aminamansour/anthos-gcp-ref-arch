output "project_id" {
  value       = module.host_project.project_id
  description = "The ID of the created project"
}

output "project" {
  value       = module.host_project
  description = "The full host project info"
}