# Copyright 2019 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

FROM gcr.io/google.com/cloudsdktool/cloud-sdk:alpine
RUN apk add -u jq python3 openssl

ENV TERRAFORM_VERSION=0.13.6
ENV KUBECTL_VERSION=1.18.0
ENV TF_PLUGIN_CACHE_DIR /workspace/.terraform.d/plugin-cache

# Install terraform
RUN wget https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
    unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
    chmod +x terraform && \
    mv terraform /usr/local/bin && \
    rm -rf terraform_${TERRAFORM_VERSION}_linux_amd64.zip

RUN gcloud components install kubectl kustomize beta alpha

#Install kpt
RUN wget https://github.com/GoogleContainerTools/kpt/releases/download/v0.39.2/kpt_linux_amd64 -O /usr/local/bin/kpt_0_39_2
RUN chmod +x /usr/local/bin/kpt_0_39_2
RUN ln -s /usr/local/bin/kpt_0_39_2 /usr/local/bin/kpt