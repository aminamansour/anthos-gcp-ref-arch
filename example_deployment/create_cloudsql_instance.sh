# Copyright 2020 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# !/bin/bash

function echo_cyan() { echo -e "${CYAN}$@${NC}"; }
function echo_green() { echo -e "${GREEN}$@${NC}"; }

grep -q "export INSTANCE_NAME=.*" ${SCRIPT_DIR}/../../vars.sh || echo -e "export INSTANCE_NAME=bank-of-anthos-db-multi" >> ${SCRIPT_DIR}/../../vars.sh

source ${SCRIPT_DIR}/../../vars.sh

echo_cyan "*** Enabling the Cloud SQL API... *** \n"
gcloud config set project ${APP_PROJECT_ID}
gcloud services enable sqladmin.googleapis.com

echo_cyan "*** Creating Cloud SQL instance: ${INSTANCE_NAME}... *** \n"
gcloud sql instances create ${INSTANCE_NAME} \
    --database-version=POSTGRES_12 --tier=db-custom-1-3840 \
    --region=${DB_REGION} --project ${APP_PROJECT_ID}

echo_cyan "*** All done creating ${INSTANCE_NAME}... ***\n"
grep -q "export INSTANCE_CONNECTION_NAME=.*" ${SCRIPT_DIR}/../../vars.sh || echo -e "export INSTANCE_CONNECTION_NAME=$(gcloud sql instances describe ${INSTANCE_NAME} --format='value(connectionName)')" >> ${SCRIPT_DIR}/../../vars.sh

echo_cyan "*** Creating admin user... ***\n"
gcloud sql users create admin \
   --instance=$INSTANCE_NAME --password=admin

# Create Accounts DB
echo_cyan "***Creating accounts-db in ${INSTANCE_NAME}...***\n"
gcloud sql databases create accounts-db --instance=$INSTANCE_NAME

# Create Ledger DB
echo_cyan "***Creating ledger-db in ${INSTANCE_NAME}... ***\n"
gcloud sql databases create ledger-db --instance=$INSTANCE_NAME
