# Copyright 2020 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#!/usr/bin/env bash

is_deployment_ready() {
kubectl --context $1 -n $2 get deploy $3 &> /dev/null
    export exit_code=$?
    while [ ! " ${exit_code} " -eq 0 ]
        do
            sleep 5
            echo -e "Waiting for deployment $3 in cluster $1 to be created..."
            kubectl --context $1 -n $2 get deploy $3 &> /dev/null
            export exit_code=$?
        done
    echo -e "Deployment $3 in cluster $1 created."

    # Once deployment is created, check for deployment status.availableReplicas is greater than 0
    export availableReplicas=$(kubectl --context $1 -n $2 get deploy $3 -o json | jq -r '.status.availableReplicas')
    while [[ " ${availableReplicas} " == " null " ]]
        do
            sleep 5
            echo -e "Waiting for deployment $3 in cluster $1 to become ready..."
            export availableReplicas=$(kubectl --context $1 -n $2 get deploy $3 -o json | jq -r '.status.availableReplicas')
        done

    echo -e "$3 in cluster $1 is ready with replicas ${availableReplicas}."
    return ${availableReplicas}
}

export CYAN='\033[1;36m'
export GREEN='\033[1;32m'
export NC='\033[0m' # No Color
function echo_cyan() { echo -e "${CYAN}$@${NC}"; }
function echo_green() { echo -e "${GREEN}$@${NC}"; }

# Export a SCRIPT_DIR var and make all links relative to SCRIPT_DIR
export SCRIPT_DIR=$(dirname $(readlink -f $0 2>/dev/null) 2>/dev/null || echo "${PWD}/$(dirname $0)")

source $SCRIPT_DIR/../scripts/user_setup.sh

# Define vars
export NAMESPACE=bofa-prod
export DB_REGION="us-west1"
export ZONE="us-west1-b"
export KSA_NAME="boa-ksa"
export GSA_NAME="boa-gsa"

echo_cyan "*** Setting up workload identity for Cloud SQL... ***\n"

## Stage 1: Preparation
# Retrieve & replace asm rev label
ASM_REV_LABEL=$(kubectl get deploy -n istio-system -l app=istiod -o jsonpath={.items[*].metadata.labels.'istio\.io\/rev'})

sed -e "s/ASM_REV_LABEL/${ASM_REV_LABEL}/" ${SCRIPT_DIR}/bank-of-anthos/bofa-namespace.yaml.tmpl > ${SCRIPT_DIR}/bank-of-anthos/bofa-namespace.yaml

echo_cyan "*** Creating ${NAMESPACE} namespace... ***\n"

kubectl --context=${GKE1} apply -f ${SCRIPT_DIR}/bank-of-anthos/bofa-namespace.yaml
kubectl --context=${GKE2} apply -f ${SCRIPT_DIR}/bank-of-anthos/bofa-namespace.yaml
kubectl --context=${GKE3} apply -f ${SCRIPT_DIR}/bank-of-anthos/bofa-namespace.yaml
kubectl --context=${GKE4} apply -f ${SCRIPT_DIR}/bank-of-anthos/bofa-namespace.yaml

echo_cyan "*** Creating GCP Service account... ***\n"
gcloud iam service-accounts create $GSA_NAME --project $OPS_PROJECT_ID

echo_cyan "*** Creating Kubernetes Service Accounts... ***\n"

kubectl --context=${GKE1} create serviceaccount --namespace $NAMESPACE "${KSA_NAME}"
kubectl --context=${GKE2} create serviceaccount --namespace $NAMESPACE "${KSA_NAME}"
kubectl --context=${GKE3} create serviceaccount --namespace $NAMESPACE "${KSA_NAME}"
kubectl --context=${GKE4} create serviceaccount --namespace $NAMESPACE "${KSA_NAME}"

echo_cyan "*** Annotating service account to connect your GSA and KSA... ***\n"
gcloud iam service-accounts add-iam-policy-binding \
  --role roles/iam.workloadIdentityUser \
  --member "serviceAccount:${OPS_PROJECT_ID}.svc.id.goog[$NAMESPACE/$KSA_NAME]" \
  ${GSA_NAME}@${OPS_PROJECT_ID}.iam.gserviceaccount.com \
  --project ${OPS_PROJECT_ID}

kubectl --context=${GKE1} annotate serviceaccount \
  --namespace ${NAMESPACE} \
  ${KSA_NAME} \
  iam.gke.io/gcp-service-account=${GSA_NAME}@${OPS_PROJECT_ID}.iam.gserviceaccount.com

kubectl --context=${GKE2} annotate serviceaccount \
  --namespace ${NAMESPACE} \
  ${KSA_NAME} \
  iam.gke.io/gcp-service-account=${GSA_NAME}@${OPS_PROJECT_ID}.iam.gserviceaccount.com

kubectl --context=${GKE3} annotate serviceaccount \
  --namespace ${NAMESPACE} \
  ${KSA_NAME} \
  iam.gke.io/gcp-service-account=${GSA_NAME}@${OPS_PROJECT_ID}.iam.gserviceaccount.com

kubectl --context=${GKE4} annotate serviceaccount \
  --namespace ${NAMESPACE} \
  ${KSA_NAME} \
  iam.gke.io/gcp-service-account=${GSA_NAME}@${OPS_PROJECT_ID}.iam.gserviceaccount.com

echo_cyan "*** Granting Service account permissions... ***"
gcloud projects add-iam-policy-binding ${OPS_PROJECT_ID} \
  --member "serviceAccount:${GSA_NAME}@${OPS_PROJECT_ID}.iam.gserviceaccount.com" \
  --role roles/cloudtrace.agent

gcloud projects add-iam-policy-binding ${OPS_PROJECT_ID} \
  --member "serviceAccount:${GSA_NAME}@${OPS_PROJECT_ID}.iam.gserviceaccount.com" \
  --role roles/monitoring.metricWriter

gcloud projects add-iam-policy-binding ${APP_PROJECT_ID} \
  --member "serviceAccount:${GSA_NAME}@${OPS_PROJECT_ID}.iam.gserviceaccount.com" \
  --role roles/cloudsql.client

echo_cyan "*** Creating Cloud SQL instance... ***\n"
source ${SCRIPT_DIR}/create_cloudsql_instance.sh

echo_cyan "*** Creating Cloud SQL admin demo secret in GKE clusters... ***\n"
kubectl --context=${GKE1} create secret -n ${NAMESPACE} generic cloud-sql-admin \
 --from-literal=username=admin --from-literal=password=admin \
 --from-literal=connectionName=${INSTANCE_CONNECTION_NAME}

kubectl --context=${GKE2} create secret -n ${NAMESPACE} generic cloud-sql-admin \
 --from-literal=username=admin --from-literal=password=admin \
 --from-literal=connectionName=${INSTANCE_CONNECTION_NAME}

kubectl --context=${GKE3} create secret -n ${NAMESPACE} generic cloud-sql-admin \
 --from-literal=username=admin --from-literal=password=admin \
 --from-literal=connectionName=${INSTANCE_CONNECTION_NAME}

kubectl --context=${GKE4} create secret -n ${NAMESPACE} generic cloud-sql-admin \
 --from-literal=username=admin --from-literal=password=admin \
 --from-literal=connectionName=${INSTANCE_CONNECTION_NAME}

echo -e "\n"
echo_cyan "*** Deploying Bank of Anthos to ${GKE1} cluster... ***\n"
kubectl --context=${GKE1} -n ${NAMESPACE} apply -k ${SCRIPT_DIR}/bank-of-anthos/gke1
echo -e "\n"
echo_cyan "*** Deploying Bank of Anthos to ${GKE2} cluster... ***\n"
kubectl --context=${GKE2} -n ${NAMESPACE} apply -k ${SCRIPT_DIR}/bank-of-anthos/gke2
echo -e "\n"
echo_cyan "*** Deploying Bank of Anthos to ${GKE3} cluster... ***\n"
kubectl --context=${GKE3} -n ${NAMESPACE} apply -k ${SCRIPT_DIR}/bank-of-anthos/gke3
echo -e "\n"
echo_cyan "*** Deploying Bank of Anthos to ${GKE4} cluster... ***\n"
kubectl --context=${GKE4} -n ${NAMESPACE} apply -k ${SCRIPT_DIR}/bank-of-anthos/gke4

echo -e "\n"
echo_cyan "*** Verifying all Deployments are Ready in all clusters... ***\n"
is_deployment_ready ${GKE1} ${NAMESPACE} contacts
is_deployment_ready ${GKE1} ${NAMESPACE} userservice

is_deployment_ready ${GKE2} ${NAMESPACE} balancereader
is_deployment_ready ${GKE2} ${NAMESPACE} ledgerwriter
is_deployment_ready ${GKE2} ${NAMESPACE} transactionhistory

is_deployment_ready ${GKE1} ${NAMESPACE} frontend
is_deployment_ready ${GKE2} ${NAMESPACE} frontend
is_deployment_ready ${GKE3} ${NAMESPACE} frontend
is_deployment_ready ${GKE4} ${NAMESPACE} frontend

is_deployment_ready ${GKE4} ${NAMESPACE} loadgenerator

echo -e "\n"
echo_cyan "*** Deploying Multicluster ingress... ***\n"
kubectl --context=${GKE3} apply -f ${SCRIPT_DIR}/bank-of-anthos/mci

echo -e "\n"
echo_green "Wait for ~10 minutes before running the next commands to ensure all ingress resources were created successfully.\n"
