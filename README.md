# Anthos Reference Architecture - GCP implementation guide



## Prerequisites

## Requirements

Please install python3 if it doesn't already exist and add its path to PATH environment variable.

## Preparing the environment

1. Create a `WORKDIR` and clone the repo inside. All related files to this tutorial end up in `WORKDIR`. This way you can delete the `WORKDIR` when you are done with testing this.
```
mkdir -p $HOME/anthos-ref-arch && cd $HOME/anthos-ref-arch
export WORKDIR=$HOME/anthos-ref-arch
git clone https://gitlab.com/aminamansour/anthos-gcp-ref-arch.git $WORKDIR/anthos-gcp
```
## Deploying the platform
1. Run the `build.sh` script in the `scripts` folder. The script will prompt you to enter your organization name and billing account id.

```
$WORKDIR/anthos-gcp/scripts/build.sh
```
> `build.sh` script is idempotent and may be run multiple times.
>> Note that the infrastructure build process can take approximately 30 - 35 minutes to complete.
  
- The `build.sh` script does the following:
    - Installs the necessary tools (kustomize, pv and kubectl krew plugin). 
    - Creates a terraform admin project and sets the necessary IAM roles and permissions to run the terraform scripts to set up the infrastructure.
    - Sets up an `infrastructure` CSR repo with a Build Trigger to build the remainder of the infrastructure using Cloud Build and terraform. 
- You can trigger this pipeline by running the build.sh script which commits the changes to the infrastructure CSR repo's main branch. Alternatively, you can directly commit changes to the infrastructure repo which is cloned in the ${WORKDIR}/infra-repo folder.
- Running the build.sh script overrides any changes you make locally through the infra-repo folder.

## Set up your environment to access the platform
1. Verify that the Cloud Build pipeline finished successfully.
```bash
gcloud builds list --project $GCP_PROJECT_ID
```
- Optional: follow progress of build with

```bash
watch -n 1 gcloud builds list --project $GCP_PROJECT_ID
```

_OUTPUT (Do not copy)_

```bash
ID                                    CREATE_TIME                DURATION  SOURCE                                                                                                      IMAGES  STATUS
ca7c4cd3-cd26-46ab-b1ba-49d93e80d567  2020-09-11T21:23:49+00:00  24M23S    gs://qwiklabs-gcp-02-3d346cf87fd8_cloudbuild/source/1599859427.742461-84b48dc9b6c94fe1b1d9f4f3c490f635.tgz  -       SUCCESS
```

- You can also view this through the **Cloudbuild** page in Cloud Console.
Run the command below to get the direct link for Cloud Build.
```
echo https://console.cloud.google.com/cloud-build/builds?project=$GCP_PROJECT_ID
```      
2. Run the `user_setup.sh` script from the repository root folder.

```bash
source ${HOME}/.bashrc # If you're using ZSH, source ${HOME}/.zshrc 
source ${WORKDIR}/anthos-gcp/scripts/user_setup.sh
```
- This `user_setup.sh` script performs the following steps:
  - Sources the `vars.sh` file so your environment variables are available.
  - Generates `KUBECONFIG` file with credentials for all 4 GKE clusters created.
    > The script is idempotent and can be run multiple times.


## Deploying the sample application
The sample application is the [Bank of Anthos](https://github.com/GoogleCloudPlatform/bank-of-anthos) sample application.
Use the script provided to do the following
- Deploy a Cloud SQL PostgreSQL database in the application team project
- Deploy the Bank of Anthos application spread across 4 clusters
- Deploy a Multicluster Ingress (MCI) to allow you to access the application through a single VIP with backends in all 4 clusters.
1. Run the `install_bofa.sh` script
```
$WORKDIR/anthos-gcp/example_deployment/install_bofa.sh
```
2. Access the application at the IP address returned from the command below.
```
kubectl --context=$GKE3 get mci istio-ingressgateway-multicluster-ingress -n istio-system -o jsonpath='{.status.VIP}'
```
> Notes: If the command returns nothing then retry after a few minutes. 
>> The MCI resources need up to 10 minutes to be fully functioning. If more than 10 minutes passed and you do not get a VIP returned from this command or you are still unable to access your application, please refer to the [Troubleshooting section](https://cloud.google.com/kubernetes-engine/docs/how-to/troubleshooting-and-ops) in documentation.
- Log in as test-user with the pre-populated credentials added to the Cloud SQL-based accounts-db. You should see the pre-populated transaction data show up, from the Cloud SQL-based ledger-db. You're done!