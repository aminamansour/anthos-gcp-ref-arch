import json
import sys
"""
Script accepts three parameters : input file(json formatted) , output file , user that needs to be added
If the json input passed has an entry for role:"roles/billing.user" and the user exists as its member, no action is taken
If the json input passed has an entry for role:"roles/billing.user" and the user doesn't exists as its member, it is added to members list
If the json input passed has no entry for role:"roles/billing.user", an entry is added with user as member and role as "roles/billing.user
"""

input = sys.argv[1]
output = sys.argv[2]
user = "serviceAccount:" + sys.argv[3]
flag=0
with open(input) as f:
  input = json.load(f)

for k,v in input.items():
  if k == "bindings":
    for list_item_dict in v:
      if 'role' in list_item_dict and list_item_dict['role']=="roles/billing.user":
        if user in list_item_dict['members']:
          flag = 1 #no change needed as the user already exists as roles/billing.user
        else:
          list_item_dict['members'].append(user)
          flag = 1 #added the user as roles/billing.user

if flag == 0: # No entry was found for roles/billing.user
  #print(input['bindings'])
  for k,v in input.items():
    if k == "bindings":
      v.append({"members":[user],"role":"roles/billing.user"})

with open(output,'w') as f:
  json.dump(input,f,indent=2)