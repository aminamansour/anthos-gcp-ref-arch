# General
export PROJECT_EDITOR=$(gcloud config get-value account)
export HOST_PROJECT_NAME='projectx-0-net-prod'
export PLATFORM_ADMIN_PROJECT_NAME='projectx-1-admin-prod'
export APP_PROJECT_NAME='projectx-2-bofa-prod'

# VPCS
export VPC_NAME='shared-vpc'

# REGIONS
export REGION1='us-central1'
export REGION2='us-west1'
export REGION3='us-west2'
export REGION4='us-east1'
export REGION5='us-east4'

# SUBNETS
export SUBNET1_NAME="10-40-0-0-22"
export SUBNET1_DESC="shared-vpc-subnet-1"
export SUBNET1_IP='10.40.0.0/22'
export POD_RANGE1_NAME='10-0-0-0-14'
export POD_RANGE1='10.0.0.0/14'
export SVC_RANGE1_NAME='10-5-0-0-20'
export SUBNET1_SVC_RANGE1='10.5.0.0/20'
export SUBNET1_SVC_RANGE1_NAME='10-5-0-0-20'
export SUBNET1_SVC_RANGE2='10.5.16.0/20'
export SUBNET1_SVC_RANGE2_NAME='10-5-16-0-20'

export SUBNET2_NAME='10-12-0-0-22'
export SUBNET2_DESC="shared-vpc-subnet-2"
export SUBNET2_IP='10.12.0.0/22'
export POD_RANGE2_NAME='10-8-0-0-14'
export POD_RANGE2='10.8.0.0/14'
export SUBNET2_SVC_RANGE1_NAME='10-13-0-0-20'
export SUBNET2_SVC_RANGE1='10.13.0.0/20'
export SUBNET2_SVC_RANGE2_NAME='10-13-16-0-20'
export SUBNET2_SVC_RANGE2='10.13.16.0/20'

