#!/usr/bin/env bash

# Copyright 2019 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# Verify that the scripts are being run from Linux and not Mac
if [[ $OSTYPE != "linux-gnu" ]]; then
    echo "ERROR: This script and consecutive set up scripts have only been tested on Linux. Currently, only Linux (debian) is supported. Please run in Cloud Shell or in a VM running Linux".
    exit;
fi

# Create a logs folder and file and send stdout and stderr to console and log file
mkdir -p ${WORK_DIR}/logs
export LOG_FILE=${WORK_DIR}/logs/tools-$(date +%s).log
touch ${LOG_FILE}
exec 2>&1
exec &> >(tee -i ${LOG_FILE})

source ${WORK_DIR}/anthos-gcp/scripts/functions.sh

# Tools

# Set speed
bold=$(tput bold)
normal=$(tput sgr0)

color='\e[1;32m' # green
nc='\e[0m'

echo -e "\n"
title_no_wait "*** TOOLS ***"
echo -e "\n"
title_no_wait "Download kustomize cli, pv and kubectl krew plugin tools."
nopv_and_execute "mkdir -p ${HOME}/bin && cd ${HOME}/bin"
export KUSTOMIZE_FILEPATH="${HOME}/bin/kustomize"
if [ -f ${KUSTOMIZE_FILEPATH} ]; then
    title_no_wait "kustomize is already installed and in the ${KUSTOMIZE_FILEPATH} folder."
else
    nopv_and_execute "curl -s \"https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh\"  | bash"
fi
export PATH=$PATH:${HOME}/bin
[[ ! -e ${HOME}/.asm.bash ]] && touch ${HOME}/.asm.bash
grep -q "export PATH=.*\${HOME}/bin.*" ${HOME}/.asm.bash || echo "export PATH=\$PATH:\${HOME}/bin" >> ${HOME}/.asm.bash

export PV_INSTALLED=`which pv`
if [ -z ${PV_INSTALLED} ]; then
    nopv_and_execute "sudo apt-get update && sudo apt-get -y install pv"
    nopv_and_execute "sudo mv /usr/bin/pv ${HOME}/bin/pv"
else
    title_no_wait "pv is already installed and in the ${PV_INSTALLED} folder."
fi

export KREW_FILEPATH="${HOME}/.krew"
if [ -d ${KREW_FILEPATH} ]; then
    title_no_wait "kubectl krew is already installed and in the ${KREW_FILEPATH} folder."
else
    nopv_and_execute "
    (
    set -x; cd \"$(mktemp -d)\" &&
    curl -fsSLO \"https://github.com/kubernetes-sigs/krew/releases/latest/download/krew.{tar.gz,yaml}\" &&
    tar zxvf krew.tar.gz &&
    ./krew-\"$(uname | tr '[:upper:]' '[:lower:]')_amd64\"  install --manifest=krew.yaml --archive=krew.tar.gz &&
    ./krew-\"$(uname | tr '[:upper:]' '[:lower:]')_amd64\" update
    )
    "
    export PATH="${PATH}:${HOME}/.krew/bin"
    grep -q "export PATH=.*\${HOME}/.krew/bin" ${HOME}/.asm.bash || echo -e "export PATH="${PATH}:${HOME}/.krew/bin"" >> ~/.asm.bash
    kubectl krew install ctx
    kubectl krew install ns
fi

grep -q ".asm.bash" ${HOME}/.bashrc || (echo "source ${HOME}/.asm.bash" >> ${HOME}/.bashrc)
