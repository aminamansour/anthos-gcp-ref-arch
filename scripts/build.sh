#!/usr/bin/env bash

# Copyright 2019 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Verify that the scripts are being run from Linux and not Mac
if [[ $OSTYPE != "linux-gnu" ]]; then
    echo "ERROR: This script and consecutive set up scripts have only been tested on Linux. Currently, only Linux (debian) is supported. Please run in Cloud Shell or in a VM running Linux".
    exit;
fi

#Set python3 path
export PYTHON=$(which python3)
if [ -z ${PYTHON} ]; then
  echo "ERROR: `which python3` did not yield any path to the executable. Please install python3 if not done already and make sure to add the directory of the executable in PATH environment variable"
  exit;
fi

export SCRIPT_DIR=$(dirname $(readlink -f $0 2>/dev/null) 2>/dev/null || echo "${PWD}/$(dirname $0)")

# Create a logs folder and file and send stdout and stderr to console and log file
mkdir -p ${SCRIPT_DIR}/../../logs
if [ ! -f ${SCRIPT_DIR}/../../vars.sh ]; then
    cp ${SCRIPT_DIR}/vars.sh ${SCRIPT_DIR}/../../vars.sh
fi
source ${SCRIPT_DIR}/../../vars.sh
export LOG_FILE=${SCRIPT_DIR}/../../logs/bootstrap-$(date +%s).log
touch ${LOG_FILE}
exec 2>&1
exec &> >(tee -i ${LOG_FILE})

source ${SCRIPT_DIR}/functions.sh

touch ${HOME}/.gcp-workshop.bash
grep -q "vars.sh" ${HOME}/.gcp-workshop.bash || (echo -e "source ${SCRIPT_DIR}/../../vars.sh" >> ${HOME}/.gcp-workshop.bash)
grep -q ".gcp-workshop.bash" ${HOME}/.bashrc || (echo "source ${HOME}/.gcp-workshop.bash" >> ${HOME}/.bashrc)

# Pin ASM version, needed for tools.sh script
grep -q "export ASM_VERSION.*" ${SCRIPT_DIR}/../../../vars.sh || echo -e "export ASM_VERSION=1.8.1-asm.5" >> ${SCRIPT_DIR}/../../../vars.sh
export ISTIOCTL_INSTALLED=`which istioctl`
if [[ ${ISTIOCTL_INSTALLED} ]]; then
  title_no_wait "istioctl is already installed."
else
  wget -O $HOME/istio-${ASM_VERSION}-linux-amd64.tar.gz https://storage.googleapis.com/gke-release/asm/istio-${ASM_VERSION}-linux-amd64.tar.gz
  tar -C $HOME -xzf $HOME/istio-${ASM_VERSION}-linux-amd64.tar.gz
  rm -rf $HOME/istio-${ASM_VERSION}-linux-amd64.tar.gz
  mv $HOME/istio-${ASM_VERSION}/bin/istioctl $HOME/.local/bin/istioctl
fi

# Ensure Org ID is defined otherwise collect
while [ -z ${ORG_NAME} ]
    do
    read -p "$(echo -e "Please provide your Organization Name (your active account must be Org Admin): ")" ORG_NAME
    done

# Validate ORG_NAME exists
ORG_ID=$(gcloud organizations list \
  --filter="display_name=${ORG_NAME}" \
  --format="value(ID)")
[ ${ORG_ID} ] || { echo "Organization with that name does not exist or you do not have correct permissions in this org."; exit; }

# Validate active user is org admin
export ADMIN_USER=$(gcloud config get-value account)
gcloud organizations get-iam-policy ${ORG_ID} --format=json | \
jq '.bindings[] | select(.role=="roles/resourcemanager.organizationAdmin")' | grep ${ADMIN_USER}  &>/dev/null

[[ $? -eq 0 ]] || { echo "Active user is not an organization admin in $ORG_NAME"; exit; }

# Ensure Billing account is defined otherwise collect
while [ -z ${BILLING_ACCOUNT_ID} ]
    do
    read -p "$(echo -e "Please provide your Billing Account ID (your active account must be Billing Account Admin): ")" BILLING_ACCOUNT_ID
    done

# Validate active user is billing admin for billing account
gcloud beta billing accounts get-iam-policy ${BILLING_ACCOUNT_ID} --format=json | \
jq '.bindings[] | select(.role=="roles/billing.admin")' | grep $ADMIN_USER &>/dev/null

[[ $? -eq 0 ]] || { echo "Active user is not an billing account billing admin in $BILLING_ACCOUNT_ID"; exit; }

if [ -z "${GCP_PROJECT_ID}" ]; then
  PROJECT_ID_SUFFIX=$(cat /dev/urandom | tr -dc 'a-z0-9' | fold -w 6 | head -n 1)
  GCP_PROJECT_ID=tf-admin-${PROJECT_ID_SUFFIX}
  FOLDER_NAME=anthos-ref-arch-${PROJECT_ID_SUFFIX}

  title_no_wait "Creating folder ${FOLDER_NAME}..."
  print_and_execute "gcloud resource-manager folders create --display-name=${FOLDER_NAME} --organization=${ORG_ID}"
  FOLDER_ID=$(gcloud resource-manager folders list --organization=${ORG_ID} --filter="display_name=${FOLDER_NAME}" --format="value(ID)")
  title_no_wait "Creating terraform admin project..."
  print_and_execute "gcloud projects create ${GCP_PROJECT_ID} \
    --folder ${FOLDER_ID} \
    --name ${GCP_PROJECT_ID} \
    --set-as-default"
fi

grep -q "export GCP_PROJECT_ID.*" ${SCRIPT_DIR}/../../vars.sh || echo -e "export GCP_PROJECT_ID=${GCP_PROJECT_ID}" >> ${SCRIPT_DIR}/../../vars.sh
grep -q "gcloud config set project.*" ${SCRIPT_DIR}/../../vars.sh || echo -e "gcloud config set project ${GCP_PROJECT_ID}" >> ${SCRIPT_DIR}/../../vars.sh


# Add WORKDIR to vars
grep -q "export WORKDIR=.*" ${SCRIPT_DIR}/../../vars.sh || echo -e "export WORKDIR=${WORKDIR}" >> ${SCRIPT_DIR}/../../vars.sh

source ${SCRIPT_DIR}/../../vars.sh

set -e

title_no_wait "Linking billing account to the terraform admin project..."
print_and_execute "gcloud beta billing projects link ${GCP_PROJECT_ID} \
--billing-account ${BILLING_ACCOUNT_ID}"

title_no_wait "Enabling APIs..."
print_and_execute "gcloud services enable cloudresourcemanager.googleapis.com \
cloudbilling.googleapis.com \
iam.googleapis.com \
compute.googleapis.com \
container.googleapis.com \
serviceusage.googleapis.com \
sourcerepo.googleapis.com \
cloudbuild.googleapis.com \
servicemanagement.googleapis.com --project $GCP_PROJECT_ID"

title_no_wait "Getting Cloudbuild Service Account..."
print_and_execute "export TF_CLOUDBUILD_SA=$(gcloud projects describe $GCP_PROJECT_ID --format='value(projectNumber)')@cloudbuild.gserviceaccount.com"

title_no_wait "Giving Cloudbuild SA project owner role"
print_and_execute "gcloud projects add-iam-policy-binding ${GCP_PROJECT_ID} \
--member serviceAccount:${TF_CLOUDBUILD_SA} \
--role roles/owner"

title_no_wait "Giving Cloudbuild SA project creator IAM role at the Org level"
print_and_execute "gcloud organizations add-iam-policy-binding ${ORG_ID} \
--member serviceAccount:${TF_CLOUDBUILD_SA} \
--role roles/resourcemanager.projectCreator"

title_no_wait "Giving Cloudbuild SA billing user IAM role at the Org level"
print_and_execute "gcloud organizations add-iam-policy-binding ${ORG_ID} \
--member serviceAccount:${TF_CLOUDBUILD_SA} \
--role roles/billing.user"

title_no_wait "Giving Cloudbuild SA compute admin IAM role at the Org level"
print_and_execute "gcloud organizations add-iam-policy-binding ${ORG_ID} \
--member serviceAccount:${TF_CLOUDBUILD_SA} \
--role roles/compute.admin"

title_no_wait "Giving cloudbuild SA billing user role for the billing account..."
mkdir -p ${WORKDIR}/tmp
print_and_execute "gcloud beta billing accounts get-iam-policy ${BILLING_ACCOUNT_ID} --format=json > ${WORKDIR}/tmp/cloudbuild_billing-iam-policy-raw.json
${PYTHON} ${SCRIPT_DIR}/updateJsonPolicy.py ${WORKDIR}/tmp/cloudbuild_billing-iam-policy-raw.json ${WORKDIR}/tmp/cloudbuild_billing-iam-policy.json ${TF_CLOUDBUILD_SA}
gcloud beta billing accounts set-iam-policy ${BILLING_ACCOUNT_ID} ${WORKDIR}/tmp/cloudbuild_billing-iam-policy.json"

if [[ $(gsutil ls -p $GCP_PROJECT_ID | grep "gs://${GCP_PROJECT_ID}/") ]]; then
    title_no_wait "Bucket gs://${GCP_PROJECT_ID} already exists."
else
    title_no_wait "Creating a GCS bucket for terraform shared states..."
    print_and_execute "gsutil mb -p ${GCP_PROJECT_ID} gs://${GCP_PROJECT_ID}"
fi

if [[ $(gsutil versioning get gs://$GCP_PROJECT_ID | grep Enabled) ]]; then
    title_no_wait "Versioning already enabled on bucket gs://${GCP_PROJECT_ID}"
else
    title_no_wait "Enabling versioning on the GCS bucket..."
    print_and_execute "gsutil versioning set on gs://${GCP_PROJECT_ID}"
fi

if [[ $(gcloud source repos list --project $GCP_PROJECT_ID | grep infrastructure) ]]; then
    title_no_wait "Source repo 'infrastructure' already exists"
else
    title_no_wait "Creating infrastructure CSR repo..."
    print_and_execute "gcloud source repos create infrastructure --project $GCP_PROJECT_ID"
fi

if [[ $(gcloud alpha builds triggers list --project $GCP_PROJECT_ID | grep push-to-master) ]]; then
    title_no_wait "Build trigger 'push-to-master' already exists."
else
    title_no_wait "Creating cloudbuild trigger for infrastructure deployment..."
    print_and_execute "gcloud alpha builds triggers create cloud-source-repositories \
    --repo='infrastructure' --description='push to master' --branch-pattern='master' \
    --build-config='cloudbuild.yaml' --project $GCP_PROJECT_ID"
fi

title_no_wait "Preparing terraform backends and shared states files..."
# Define an array of GCP resources
declare -a folders
folders=(
    'gcp/prod/provider'
    'gcp/prod/gcp'
    'network/prod/shared_vpc'
    'network/prod/host_project'
    'ops/prod/platform_admin_project'
    'apps/prod/app_project'
    'ops/prod/gke_clusters'
    'ops/prod/asm'
    'ops/prod/acm_repo'
    'ops/prod/mci'
    )

# Persist vars
grep -q "export BILLING_ACCOUNT_ID=.*" ${SCRIPT_DIR}/../../vars.sh || echo -e "export BILLING_ACCOUNT_ID=${BILLING_ACCOUNT_ID}" >> ${SCRIPT_DIR}/../../vars.sh
grep -q "export ORG_NAME=.*" ${SCRIPT_DIR}/../../vars.sh || echo -e "export ORG_NAME=${ORG_NAME}" >> ${SCRIPT_DIR}/../../vars.sh
grep -q "export ORG_ID=.*" ${SCRIPT_DIR}/../../vars.sh || echo -e "export ORG_ID=${ORG_ID}" >> ${SCRIPT_DIR}/../../vars.sh
grep -q "export GCP_PROJECT_ID=.*" ${SCRIPT_DIR}/../../vars.sh || echo -e "export GCP_PROJECT_ID=${GCP_PROJECT_ID}" >> ${SCRIPT_DIR}/../../vars.sh
grep -q "export FOLDER_NAME=.*" ${SCRIPT_DIR}/../../vars.sh || echo -e "export FOLDER_NAME=${FOLDER_NAME}" >> ${SCRIPT_DIR}/../../vars.sh
grep -q "export FOLDER_ID=.*" ${SCRIPT_DIR}/../../vars.sh || echo -e "export FOLDER_ID=${FOLDER_ID}" >> ${SCRIPT_DIR}/../../vars.sh


source ${SCRIPT_DIR}/../../vars.sh

# Build backends and shared states for each GCP prod resource
for idx in ${!folders[@]}
do
    # Extract the resource name from the folder
    resource=$(echo ${folders[idx]} | grep -oP '([^\/]+$)')
    environ=$(echo ${folders[idx]} | cut -d'/' -f2)

    # Create backends
    sed -e s/PROJECT_ID/${GCP_PROJECT_ID}/ -e s/ENV/${environ}/ -e s/RESOURCE/${resource}/ \
    ${WORKDIR}/anthos-gcp/infrastructure/templates/backend.tf_tmpl > ${WORKDIR}/anthos-gcp/infrastructure/${folders[idx]}/backend.tf

    # Create shared states for every resource
    sed -e s/PROJECT_ID/${GCP_PROJECT_ID}/ -e s/ENV/${environ}/ -e s/RESOURCE/${resource}/ \
    ${WORKDIR}/anthos-gcp/infrastructure/templates/shared_state.tf_tmpl > ${WORKDIR}/anthos-gcp/infrastructure/gcp/${environ}/shared_states/shared_state_${resource}.tf

    # Create vars from terraform.tfvars_tmpl files
    tfvar_tmpl_file=${WORKDIR}/anthos-gcp/infrastructure/${folders[idx]}/terraform.tfvars_tmpl
    if [ -f "$tfvar_tmpl_file" ]; then
        envsubst < ${WORKDIR}/anthos-gcp/infrastructure/${folders[idx]}/terraform.tfvars_tmpl \
        > ${WORKDIR}/anthos-gcp/infrastructure/${folders[idx]}/terraform.tfvars
    fi

    # Create vars from variables.auto.tfvars_tmpl files
    auto_tfvar_tmpl_file=${WORKDIR}/anthos-gcp/infrastructure/${folders[idx]}/variables.auto.tfvars_tmpl
    if [ -f "$auto_tfvar_tmpl_file" ]; then
        envsubst < ${WORKDIR}/anthos-gcp/infrastructure/${folders[idx]}/variables.auto.tfvars_tmpl \
        > ${WORKDIR}/anthos-gcp/infrastructure/${folders[idx]}/variables.auto.tfvars
    fi

done

title_no_wait "Committing infrastructure terraform to cloud source repo..."
if [ -d ${WORKDIR}/infra-repo ]; then
    print_and_execute "
    cp -r ${WORKDIR}/anthos-gcp/infrastructure/* ${WORKDIR}/infra-repo
    cd ${WORKDIR}/infra-repo
    git add . && git commit -am 'commit'
    git push --set-upstream infra master
    "
else
    print_and_execute "
    mkdir -p ${WORKDIR}/infra-repo
    cp -r ${WORKDIR}/anthos-gcp/infrastructure/* ${WORKDIR}/infra-repo
    cd ${WORKDIR}/infra-repo
    git init
    git config --local user.email ${TF_CLOUDBUILD_SA}
    git config --local user.name 'terraform'
    git config --local credential.'https://source.developers.google.com'.helper gcloud.sh
    git remote add infra https://source.developers.google.com/p/$GCP_PROJECT_ID/r/infrastructure
    git add . && git commit -am 'first commit'
    git push --set-upstream infra master
    "
fi
