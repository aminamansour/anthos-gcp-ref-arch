#!/usr/bin/env bash
# Copyright 2020 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


export CYAN='\033[1;36m'
export GREEN='\033[1;32m'
export NC='\033[0m' # No Color
function echo_cyan() { echo -e "${CYAN}$@${NC}"; }
function echo_green() { echo -e "${GREEN}$@${NC}"; }

# Set WORKDIR var is not set
export WORKDIR=${WORKDIR:-$HOME/anthos-ref-arch}
echo -e "WORKDIR set to $WORKDIR"

source ${WORKDIR}/vars.sh  
  
grep -q "export GKE1=.*" ${WORKDIR}/vars.sh || echo -e "export GKE1=gke-prod-us-central1a-1" >> ${WORKDIR}/vars.sh
grep -q "export GKE2=.*" ${WORKDIR}/vars.sh || echo -e "export GKE2=gke-prod-us-central1b-2" >> ${WORKDIR}/vars.sh
grep -q "export GKE3=.*" ${WORKDIR}/vars.sh || echo -e "export GKE3=gke-prod-us-west1a-1" >> ${WORKDIR}/vars.sh
grep -q "export GKE4=.*" ${WORKDIR}/vars.sh || echo -e "export GKE4=gke-prod-us-west1b-2" >> ${WORKDIR}/vars.sh

# Get cluster project if not set
grep -q "export OPS_PROJECT_ID=.*" ${WORKDIR}/vars.sh || echo -e "export OPS_PROJECT_ID=$(gcloud projects list --filter "parent.id=${FOLDER_ID} AND parent.type=folder" --format='value(projectId)' | grep 'admin-prod')" >> ${WORKDIR}/vars.sh
# Get apps project if now set
grep -q "export APP_PROJECT_ID=.*" ${WORKDIR}/vars.sh || echo -e "export APP_PROJECT_ID=$(gcloud projects list --filter "parent.id=${FOLDER_ID} AND parent.type=folder" --format='value(projectId)' | grep 'bofa-prod')" >> ${WORKDIR}/vars.sh

source ${WORKDIR}/vars.sh

gcloud container clusters get-credentials $GKE1 --zone us-central1-a --project ${OPS_PROJECT_ID}
gcloud container clusters get-credentials $GKE2 --zone us-central1-b --project ${OPS_PROJECT_ID}
gcloud container clusters get-credentials $GKE3 --zone us-west1-a --project ${OPS_PROJECT_ID}
gcloud container clusters get-credentials $GKE4 --zone us-west1-b --project ${OPS_PROJECT_ID}

kubectl ctx $GKE1=gke_${OPS_PROJECT_ID}_us-central1-a_$GKE1
kubectl ctx $GKE2=gke_${OPS_PROJECT_ID}_us-central1-b_$GKE2
kubectl ctx $GKE3=gke_${OPS_PROJECT_ID}_us-west1-a_$GKE3
kubectl ctx $GKE4=gke_${OPS_PROJECT_ID}_us-west1-b_$GKE4
